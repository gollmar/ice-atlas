listen: the whir & the buzz
of all kinds of insects
colors the air just above
and beyond where we stand.
